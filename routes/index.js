const express = require('express');
const router = express.Router();
const projectController = require('../controller/projectController');

module.exports = function() {

    //Ruta ejemplo para el home
    router.get('/', projectController.homeProject);
    router.get('/nuevo-proyecto', projectController.formularioNuevoProyecto);
    router.post('/nuevo-proyecto', projectController.nuevoProyecto)


    return router;
}
