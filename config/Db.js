const Sequelize = require('sequelize');

// Configuración conexión a Sequelize
const db = new Sequelize('uptasknode', 'root', 'root', {
  host: 'localhost',
  dialect: 'mysql',
  operatorsAliases: false,
  define: {
    timestamps: false
  },

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },

  //SQLite only
  storage: 'path/to/database.sqlite'
});

module.exports = db;
