//express no soporta el import clasico de import express from 'express'
//Lo incorporamos con esta notacion equivalente
const express = require('express');
const routes = require('./routes');
//importo path para leer el file system (ya instalado en node)
const path = require('path');
const bodyParser = require('body-parser');

//Crear la conexión a la base de datos
const db = require('./config/Db');

//Importar el modelo
require('./models/Proyectos');

//Conexión de config a mysql
db.sync()
    .then( () => console.log('conectado al servidor') )
    .catch( error => console.log(error) );

//Creo una app de express
const app = express();

//Donde cargar los archivos estaticos
app.use(express.static('public'));

//Habilitar pug como template engine
app.set('view engine', 'pug');

//Añadir la carpeta de las vistas
app.set('views', path.join(__dirname, './views'));

//Habilitar bodyParser para leer los datos del formulario
app.use(bodyParser.urlencoded({extended: true}));

app.use('/', routes());

//Abro el servidor en el puerto 3000
app.listen(3000);
