const Proyectos = require('../models/Proyectos');

exports.homeProject = (request, response) => {
   response.render('index', {
      nombrePagina: 'Proyectos'
   });
}

exports.formularioNuevoProyecto = (request, response) => {
   response.render('nuevoProyecto', {
      nombrePagina: 'Nuevo Proyecto'
   });
}

exports.nuevoProyecto = (request, response) => {

    const { nombre } = request.body;

    //Validación mostrando errores en la vista
    let errores = [];
    if(!nombre) {
        errores.push({'texto': 'Debe agregar un nombre al proyecto'});
    }

    if(errores.length > 0) {
        response.render('nuevoProyecto', {
            nombrePagina: 'Nuevo Proyecto',
            errores
        });
    } else {
        Proyectos.create({ nombre })
            .then( () => console.log('registro creado') )
            .catch( error => console.log(error) );
    }
}
