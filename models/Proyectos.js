const Sequelize = require('sequelize');
const db = require('../config/Db');

const Proyectos = db.define('Proyectos', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    nombre: Sequelize.STRING,
    url: Sequelize.STRING
});

module.exports = Proyectos;
